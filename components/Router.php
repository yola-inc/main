<?php
    Class Router
    {
        private $routes;

        public function __construct()
        {
            $path = ROOT . "/config/routes.php";
            $this->routes = include($path);
        }

        public function run()
        {
            $uri = $this->getURI();

            $this->runAction($uri);
        }

        public function getURI()
        {
            $uri = $_SERVER['REQUEST_URI'];

            if(!empty($uri))
            {
                $uri = trim($uri ,'/');
            }

            return $uri;
        }

        public function runAction($uri)
        {
            foreach($this->routes as $uriPattern => $path)
            {
                if(preg_match("~{$uriPattern}~", $uri))
                {
                    $internalRoute = preg_replace("~{$uriPattern}~", $path, $uri);
                    $segments = explode('/', $internalRoute);

                    $controllerName = array_shift($segments) . "Controller";
                    $controllerName = ucfirst($controllerName);

                    $actionName = "action" . ucfirst(array_shift($segments));

                    $parameters = $segments;

                    $controllerFile = ROOT . "/controllers/" . $controllerName . ".php";

                    if(file_exists($controllerFile))
                    {
                        include_once($controllerFile);
                    }

                    $controllerObject = new $controllerName;
                    $action = $controllerObject->$actionName($parameters);
                    if($action != null)
                    {
                        break;
                    }
                }
            }
        }

    }