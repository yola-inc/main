﻿<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">Header</div>
        </div>
    </div>
</div>
<div class="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">First</div>
            <div class="col-lg-4">Second</div>
            <div class="col-lg-4">Third $$$$</div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">Footer</div>
        </div>
    </div>
</div>
</body>
</html>